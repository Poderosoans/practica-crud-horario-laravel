<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Report;
use Carbon\Carbon;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $report = Report::orderBy('id', 'desc')->paginate(12);
        return view('lista', compact('report'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $report = new Report();
        $report->nombre = $request->nombre;
        $report->fechaingreso = $request->fechaingreso;
        $report->fechasalida = $request->fechasalida;

        $start = Carbon::parse($request->fechaingreso);
        $end = Carbon::parse($request->fechasalida);
        $length = $start->diffInMinutes($end);
        //obtener de otra manera
        $report->diferencia = $length;

        if($report->save()){
            return back()->with('msj','Datos enviados correctamente');
        }else{
            return back()->with('msj','Error al enviar datos');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $report = Report::find($id);
        return view('reporte', compact('report'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $report = Report::find($id);
        $report->nombre = $request->nombre;
        $report->fechaingreso = $request->fechaingreso;
        $report->fechasalida = $request->fechasalida;

        $start = Carbon::parse($request->fechaingreso);
        $end = Carbon::parse($request->fechasalida);
        $length = $start->diffInMinutes($end);
        //obtener de otra manera
        $report->diferencia = $length;

        if($report->save()){
            return back();
        }else{
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Report::destroy($id);

        return back();
    }
}
