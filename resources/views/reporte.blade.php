<!doctype html>

<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>

<div class="content">
                <div class="title m-b-md">
                    Formulario
                </div>
                @if(isset($report))
                <form action="{{route('report.update',$report->id)}}" method="POST" name="myform">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <input type="text" name="nombre" value="{{$report->nombre}}">
                        <input type="text" name="fechaingreso" value="{{$report->fechaingreso}}">
                        <input type="text" name="fechasalida" value="{{$report->fechasalida}}">

                        <button type="input">Modificar datos</button>
                </form>
                @endif
                
            </div>


</body>
</html>
