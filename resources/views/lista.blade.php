<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="">
            <div class="content">
                <div class="title m-b-md">
                    Lista de datos
                </div>
                
                @if(isset($report))
                    <table border="1">
                        <thead>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Diferencia en minutos</th>
                        </thead>
                        <tbody>
                    @foreach($report as $r)
                        <tr>
                            <td>{{$r->id}}</td>
                            <td>{{$r->nombre}}</td>
                            <td>{{$r->fechaingreso}}</td>
                            <td>{{$r->fechasalida}}</td>
                            <td>{{$r->diferencia}}</td>
                            <td><a href="/report/{{$r->id}}/edit">Editar</a></td>
                            <td>
                                    <form class="deleteform" action="{{route('report.destroy', $r->id)}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                  {{ csrf_field() }}
                                 <input type="submit" class="btn btn-danger btn-xs clbutton" value="Eliminar">
                      </form>

                      </td>
                        </tr>
                    @endforeach
                    </tbody>
                    </table>
                @endif
                
            </div>
        </div>
    </body>
</html>
